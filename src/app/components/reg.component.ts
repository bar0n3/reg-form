import { Component } from '@angular/core';

@Component({
  selector: 'reg',
  templateUrl: './reg.component.html',
  styleUrls: [ './reg.component.css']
})
export class RegComponent  { 
	onSubmit(user: any) {
		alert('Cпасибо за регистрацию!');
		localStorage.setItem('userData', JSON.stringify(user));
		console.log(user);
	}
}
